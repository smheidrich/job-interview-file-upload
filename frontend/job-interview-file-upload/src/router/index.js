import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Images from '../views/Images.vue'
import Tags from '../views/Tags.vue'
import Upload from '../views/Upload.vue'
import TagView from '../views/TagView.vue'
import ImageView from '../views/ImageView.vue'

const routes = [
  /* NavBar routes (top-level) */
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      'icon': 'home',
      'inNavBar': true,
    },
  },
  {
    path: '/images',
    name: 'Images',
    component: Images,
    meta: {
      'icon': 'collections',
      'inNavBar': true,
    },
  },
  {
    path: '/tags',
    name: 'Tags',
    component: Tags,
    meta: {
      'icon': 'label',
      'inNavBar': true,
    },
  },
  {
    path: '/upload',
    name: 'Upload',
    component: Upload,
    meta: {
      'icon': 'upload_file',
      'inNavBar': true,
    },
  },
  /* Sub-views (not in NavBar) */
  {
    path: '/tags/:tag',
    component: TagView,
  },
  {
    path: '/images/:imageId',
    component: ImageView,
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
