# job-interview-file-upload

To run Dockerized:

1. Place desired PostgreSQL password in `secrets/db/password`
2. Place desired Thumbor security key in `secrets/thumbor/security_key`
3. Run with `sudo docker-compose -f docker-compose-dev.yml up`

