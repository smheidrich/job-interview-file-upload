from .config import settings
from pony.orm import Database, Optional, Required, Set, select

db = Database()


# models

class Image(db.Entity):
  filename = Required(str)
  name = Optional(str)
  tags = Set(lambda: Tag)
  suggested_tags = Set(lambda: Tag, reverse="suggested_images")


class Tag(db.Entity):
  text = Required(str, unique=True)
  images = Set(Image)
  suggested_images = Set(Image, reverse="suggested_tags")


# helpers

def get_tag(text):
  return select(x for x in Tag if x.text == text).get()


def get_or_create_tag(text):
  tag = get_tag(text)
  if tag is None:
    tag = db.Tag(text=text)
  return tag


# binding

POSTGRES_PASSWORD = settings.postgres_password_path.read_text().strip()

db.bind(provider='postgres', user="postgres", password=POSTGRES_PASSWORD,
  host="db")
db.generate_mapping(create_tables=True)
