from fastapi import FastAPI, File, HTTPException, UploadFile, Body
from fastapi.staticfiles import StaticFiles
from pydantic import BaseModel, Field, constr, conint
from uuid import uuid4
import traceback
from typing import List
from warnings import warn

from pony.orm import db_session, select, desc, count
from . import database as db
from .config import settings
from .utils.image import verify_image_and_get_suffix
from .utils.temp import temp_to_named_file
from .utils.tag_suggestions import suggest_tags
from .utils.thumbnail_urls import image_urls

app = FastAPI()


# models

class Image(BaseModel):
  id: int
  full_size_url: str
  screen_size_url: str
  thumbnail_url: str


class ImageWithTags(Image):
  tags: List[str] = Field(default_factory=list)
  suggested_tags: List[str] = Field(default_factory=list)


class PopularTag(BaseModel):
  name: str
  n_images: int
  sample_images: List[Image]


# utils

def get_image_by_id_or_404(image_id):
  """
  Needs to be run within a pony session.
  """
  image = select(x for x in db.Image if x.id == image_id).get()
  if image is None:
    raise HTTPException(
      status_code=404,
      detail=f"Image with id {image_id} not found"
    )
  return image


TagStr = constr(strip_whitespace=True, to_lower=True, min_length=1,
  max_length=200)


# routes

@app.get("/images", response_model=List[Image])
async def read_images(limit: conint(le=16)=16, offset: int=0):
  with db_session:
    return [
      Image(id=x.id, **image_urls(x))
      for x in select(x for x in db.Image).limit(limit=limit, offset=offset)
    ]


@app.get("/images/{image_id}", response_model=ImageWithTags)
async def read_image(image_id: int):
  with db_session:
    image = select(x for x in db.Image if x.id == image_id).get()
    return ImageWithTags(
      id=image.id,
      tags=list(tag.text for tag in image.tags),
      suggested_tags=list(tag.text for tag in image.suggested_tags),
      **image_urls(image),
    )


@app.post("/images/", response_model=ImageWithTags)
def create_image(file: UploadFile = File(...)):
  # determine file type (ignore whatever was specified by header / filename)
  # and check that image is valid (consumes some memory and takes some time)
  suffix = verify_image_and_get_suffix(file.file)
  if suffix is None:
    raise HTTPException(status_code=415, detail="Invalid or corrupted image")

  target_filename = f"{uuid4().hex}{suffix}"
  target_path = settings.file_storage_path/target_filename
  temp_to_named_file(file.file, target_path)

  # ML tag suggestion, also massively CPU-bound
  suggested_tags = suggest_tags(target_path)

  with db_session():
    suggested_tags_db = [
      db.get_or_create_tag(tag) for tag in suggested_tags
    ]
    image = db.Image(filename=target_filename,
      suggested_tags=suggested_tags_db)

  return ImageWithTags(id=image.id, name=file.filename,
    suggested_tags=suggested_tags, **image_urls(image))


@app.delete("/images/{image_id}")
def delete_image(image_id: int):
  with db_session:
    image = get_image_by_id_or_404(image_id)
    try:
      (settings.file_storage_path/image.filename).unlink()
    except Exception:
      warn(f"error while deleting image with id {image_id}:")
      traceback.print_exc()
      warn("deleting from database anyway")
    image.delete()
  return { "id": image_id }


@app.post("/images/{image_id}/tags")
def add_image_tag(image_id, tag: TagStr = Body(...)) -> str:
  tag_text = tag.lower().strip() # normalize
  with db_session():
    image = get_image_by_id_or_404(image_id)
    tag = db.get_or_create_tag(tag_text)
    image.tags.add(tag)
    if tag in image.suggested_tags:
      image.suggested_tags.remove(tag)
  return tag_text


@app.delete("/images/{image_id}/tags/{tag}")
def delete_image_tag(image_id: int, tag: TagStr):
  tag_text = tag
  with db_session():
    image = get_image_by_id_or_404(image_id)
    tag = db.get_tag(tag_text)
    if tag is None or tag not in image.tags:
      raise HTTPException(
        status_code=404,
        detail=f"Tag '{tag_text}' not in image {image_id}'s tags"
      )
    image.tags.remove(tag)
  return { "image_id": image_id, "tag": tag }


@app.delete("/images/{image_id}/suggested_tags/{tag}")
def delete_image_suggested_tag(image_id: int, tag: TagStr):
  tag_text = tag
  with db_session():
    image = get_image_by_id_or_404(image_id)
    tag = db.get_tag(tag_text)
    if tag is None or tag not in image.suggested_tags:
      raise HTTPException(
        status_code=404,
        detail=f"Tag '{tag_text}' not in image {image_id}'s suggested tags"
      )
    image.suggested_tags.remove(tag)
  return { "image_id": image_id, "tag": tag }


@app.get("/tags", response_model=List[str])
async def read_tags():
  with db_session:
    return [
      x.text for x in select(x for x in db.Tag)
    ]


@app.get("/tags/{tag_name}/images", response_model=List[Image])
async def read_tag_images(tag_name, limit: conint(le=16)=16, offset: int=0):
  with db_session:
    tag = db.get_tag(tag_name)
    return [
      Image(id=x.id, **image_urls(x)) for x
      in tag.images.limit(limit=limit, offset=offset)
    ]


@app.get("/popular_tags", response_model=List[PopularTag])
async def read_popular_tags():
  with db_session:
    return [
      PopularTag(
        name=tag.text,
        n_images=len(tag.images), # TODO does this realize them all in memory?
        sample_images=[
          Image(id=image.id, **image_urls(image))
          for image in tag.images.limit(4)
        ]
      )
      for tag in select(tag for tag in db.Tag)
      .order_by(lambda tag: desc(count(tag.images)))
      .limit(5)
    ]


app.mount(
  settings.file_storage_url,
  StaticFiles(directory=settings.file_storage_path),
  name="uploaded"
)
