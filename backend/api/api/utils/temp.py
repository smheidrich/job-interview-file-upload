import shutil


def temp_to_named_file(temp_file, path, chunk_size=1024):
  """
  Writes the contents of a (Spooled)TemporaryFile to a named file
  """
  # stolen from: https://stackoverflow.com/a/63581187
  # NOTE: the copying can be avoided and replaced by a move (conditional on the
  # temp dir residing on the same fs as the output dir) once
  # https://github.com/tiangolo/fastapi/issues/1838 is implemented
  with path.open("wb") as out_file:
    shutil.copyfileobj(temp_file, out_file)
