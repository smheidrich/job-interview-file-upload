from PIL import Image
from mimetypes import guess_extension


def verify_image_and_get_suffix(file):
  try:
    image = Image.open(file)
    image.verify()
  except:
    return None
  mimetype = image.get_format_mimetype()
  suffix = guess_extension(mimetype)
  file.seek(0)
  return suffix
