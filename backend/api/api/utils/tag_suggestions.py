from tensorflow.keras.applications.resnet50 import ResNet50
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.resnet50 import preprocess_input, decode_predictions
import numpy as np


model = ResNet50(weights='imagenet')

def suggest_tags(image_path, n=3) -> set[str]:
  img = image.load_img(image_path, target_size=(224, 224))
  x = image.img_to_array(img)
  x = np.expand_dims(x, axis=0)
  x = preprocess_input(x)

  preds = model.predict(x)

  predicted = decode_predictions(preds, top=n)[0] # (class, tag_name, prob)

  return set(x[1].lower().replace("_", " ") for x in predicted)
