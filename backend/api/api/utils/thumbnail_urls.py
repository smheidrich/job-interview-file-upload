from ..config import settings
from libthumbor import CryptoURL

SECURITY_KEY = None # will be loaded on first call to image_urls


def image_internal_url(db_image):
  """
  Get non-thumbor (i.e. raw storage) URL for image in DB
  """
  return f"{settings.thumbor_file_storage_prefix}/{db_image.filename}"


def image_urls(db_image):
  """
  Generate all relevant Thumbor URLs for image in DB
  """
  # load security key (only once, then use cached)
  global SECURITY_KEY
  if SECURITY_KEY is None:
    SECURITY_KEY = settings.thumbor_security_key_path.read_text().strip()
  crypto_url = CryptoURL(key=SECURITY_KEY)
  # options for different thumbnail / image URLs
  url_options = dict(
    full_size_url=dict(),
    thumbnail_url=dict(fit_in=True, width=200, height=200),
    screen_size_url=dict(fit_in=True, width=800, height=800),
  )
  # generate URLs
  d = {
    k: settings.thumbor_global_prefix+crypto_url.generate(
      image_url=image_internal_url(db_image),
      **v
    ) for k, v in url_options.items()
  }
  return d
