"""
Run in order to automatically set up local directories etc.
"""
from .config import settings

if __name__ == "__main__":
  settings.file_storage_path.mkdir(exist_ok=True)
