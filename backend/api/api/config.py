from pathlib import Path
from pydantic import BaseSettings


class Settings(BaseSettings):
  file_storage_path: Path = "uploaded"
  file_storage_url: str = "/uploaded"
  thumbor_global_prefix: str = "/thumbnails"
  thumbor_file_storage_prefix: str = "uploaded"
  thumbor_security_key_path: Path = "/secrets/thumbor/security_key"
  postgres_password_path: Path = "/secrets/db/password"


settings = Settings()
