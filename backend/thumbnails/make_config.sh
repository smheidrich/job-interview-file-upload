#!/bin/sh
set -e
echo -n "SECURITY_KEY = '" > thumbor.conf
cat /secrets/thumbor/security_key | tr -d '\n\r' >> thumbor.conf
echo "'" >> thumbor.conf
