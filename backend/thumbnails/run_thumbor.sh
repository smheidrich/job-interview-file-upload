#!/bin/sh
set -e
echo "Creating Thumbor config..."
./make_config.sh
echo "Starting Thumbor..."
thumbor --use-environment True -c thumbor.conf
